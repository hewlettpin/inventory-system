package sample.Models;

public class EvaluationCrates {
    public String date;
    public double amount;

    public EvaluationCrates(String date, double amount) {
        this.date = date;
        this.amount = amount;
    }
}
