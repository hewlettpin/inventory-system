package sample;

import com.jfoenix.controls.JFXButton;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.json.JSONException;
import org.json.JSONObject;
import sample.Models.LoginResult;
import sample.Backend.DatabaseHandler;
import java.io.IOException;

public class LoginController {

    @FXML
    private JFXButton actionBtn;
    @FXML public TextField usernameTxtField;
    @FXML public PasswordField passwordTxtField;
    @FXML public Text actiontarget;

    DatabaseHandler db;
    JSONObject userObject;

    public void onClick(){
        String username = usernameTxtField.getText();
        String password = passwordTxtField.getText();

        if(username.equals("")||password.equals("")){
            actiontarget.setText("Please fill all the fields");
        }else {
            if(Checker.isStringInt(username)){
                actiontarget.setText("Please check your username");
                passwordTxtField.setText("");
            }else {
                Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), ev -> {
//                    MessageDigest md = null;
//                    try {
//                        md = MessageDigest.getInstance("MD5");
//                    } catch (NoSuchAlgorithmException e) {
//                        e.printStackTrace();
//                    }
//                    md.update(password.getBytes());
//
//                    byte byteData[] = md.digest();
//
//                    //convert the byte to hex format method 1
//                    StringBuffer sb = new StringBuffer();
//                    for (int i = 0; i < byteData.length; i++) {
//                        sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
//                    } sb.toString()

                    db = new DatabaseHandler();
                    LoginResult result = db.login(username,password);
                    if (result.success){
                        try{
                            userObject = result.userCredentials;
                            if(userObject.get("status").equals("active")){
                                Stage stage = (Stage) actionBtn.getScene().getWindow();
                                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/main_window.fxml"));
                                Scene MainScene = new Scene(fxmlLoader.load(), 1300, 800);
                                stage.setTitle("Pepsi Inventory");
                                stage.setScene(MainScene);
                                stage.setMaximized(false);
                                MainWindowController mainWindowController = fxmlLoader.getController();
                                mainWindowController.getUserDetails(userObject);
                                stage.show();
                            }else{
                                actiontarget.setText("You have no access!");
                                passwordTxtField.setText("");
                            }

                        }catch (IOException e){
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        actiontarget.setText("Please check your login credentials");
                        passwordTxtField.setText("");
                    }
                }));
                timeline.play();
            }
        }
    }
}
