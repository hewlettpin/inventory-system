package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.Backend.DatabaseHandler;
import sample.Models.EditEmployee;
import sample.Models.Expense;

import java.time.LocalDate;

public class EmployeeEditController {

    @FXML public TextField fname;
    @FXML public TextField salary;
    @FXML public TextField lname;
    @FXML public ChoiceBox<String> store_choicebox;

    DatabaseHandler db;
    int id;
    FXMLLoader fxmlLoader;

    public void getId(int id,FXMLLoader fxmlLoader){
        db = new DatabaseHandler();
        EditEmployee editEmployee = db.viewEmployee(id);
        this.id = id;
        this.fxmlLoader = fxmlLoader;
        fname.setText(editEmployee.fname);
        salary.setText(editEmployee.salary);
        lname.setText(editEmployee.lname);

        ObservableList<String> stores = FXCollections.observableArrayList(db.loadStores());
        store_choicebox.setValue(editEmployee.store);
        store_choicebox.setItems(stores);

        fname.requestFocus();
    }

    public void editEmployee(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);

        if(fname.getText().isEmpty()||lname.getText().isEmpty()||salary.getText().isEmpty()){
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }else {
            String sal = salary.getText();

            String[] amountString = sal.split(",");
            String amounter = "";
            for(int i=0;i<amountString.length;i++){
                amounter = amounter + amountString[i];
            }
            double amont = Double.parseDouble(amounter);

            DatabaseHandler db = new DatabaseHandler();
            if(db.editEmployee(id,fname.getText(),lname.getText(),amont,store_choicebox.getValue())){
                alert.setContentText("Successful Edited!");
            }else {
                alert.setContentText("UnSuccessful try Again!");
            }
            alert.showAndWait();
            ManagementController managementController = fxmlLoader.getController();
            managementController.initializeEmployees();
            Stage stage = (Stage) fname.getScene().getWindow();
            stage.close();
        }
    }
}
