package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.json.JSONException;
import org.json.JSONObject;
import sample.Backend.DatabaseHandler;
import sample.Models.Employee;
import sample.Models.Expense;
import sample.Models.User;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

public class ManagementController implements Initializable{

    //Expenses
    @FXML public TableView<Expense> exp_table;
    @FXML public TableColumn<Expense,String> exp_name;
    @FXML public TableColumn<Expense,String> exp_store;
    @FXML public TableColumn<Expense,String> exp_amount;
    @FXML public TableColumn<Expense,String> exp_desc;
    @FXML public TableColumn<Expense,String> exp_date;
    @FXML public TableColumn exp_action;
    @FXML public TextField searchExpTxt;
    @FXML public ChoiceBox<String> store_choicebox;
    @FXML public TextField expense_name;
    @FXML public TextField amount;
    @FXML public TextArea description;
    @FXML public DatePicker datePicker;

    //Users
    @FXML public TableView<User> user_table;
    @FXML public TableColumn<User,String> user_no;
    @FXML public TableColumn<User,String> user_name;
    @FXML public TableColumn<User,String> user_role;
    @FXML public TableColumn<User,String> user_status;
    @FXML public TableColumn user_action;
    @FXML public TextField user_fname;
    @FXML public TextField user_lname;
    @FXML public TextField username;
    @FXML public ChoiceBox<String> user_role_choicebox;
    @FXML public RadioButton user_male;
    @FXML public RadioButton user_female;

    //Employees
    @FXML public TableView<Employee> emp_table;
    @FXML public TableColumn<Employee,String> emp_no;
    @FXML public TableColumn<Employee,String> emp_name;
    @FXML public TableColumn<Employee,String> emp_salary;
    @FXML public TableColumn<Employee,String> emp_store;
    @FXML public TableColumn<Employee,String> emp_lastpay;
    @FXML public TableColumn<Employee,String> emp_status;
    @FXML public TableColumn emp_action;
    @FXML public TextField emp_fname;
    @FXML public TextField emp_lname;
    @FXML public TextField emp_amount;
    @FXML public ChoiceBox<String> emp_store_choicebox;
    @FXML public RadioButton emp_male;
    @FXML public RadioButton emp_female;

    ToggleGroup user_genderChoice = new ToggleGroup();
    ToggleGroup emp_genderChoice = new ToggleGroup();

    DatabaseHandler db;
    JSONObject userObject;

    ObservableList<Expense> expenses = FXCollections.observableArrayList();

    FXMLLoader fxmlLoader;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        db = new DatabaseHandler();

        initializeExpenses();

        initializeEmployees();

        ObservableList<String> stores = FXCollections.observableArrayList(db.loadStores());
        stores.add(0,"Choose Store");
        store_choicebox.setValue("Choose Store");
        store_choicebox.setItems(stores);
        emp_store_choicebox.setValue("Choose Store");
        emp_store_choicebox.setItems(stores);

        user_male.setToggleGroup(user_genderChoice);
        user_female.setToggleGroup(user_genderChoice);

        emp_male.setToggleGroup(emp_genderChoice);
        emp_female.setToggleGroup(emp_genderChoice);
    }

    public void initializeExpenses(){
        exp_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        exp_store.setCellValueFactory(new PropertyValueFactory<>("store"));
        exp_amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        exp_desc.setCellValueFactory(new PropertyValueFactory<>("description"));
        exp_date.setCellValueFactory(new PropertyValueFactory<>("date"));
        exp_action.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));

        setUpExpenses();

        DatabaseHandler db = new DatabaseHandler();
        exp_table.setItems(null);

        Task<ObservableList<Expense>> loadDataTask = new Task<ObservableList<Expense>>() {
            @Override
            protected ObservableList<Expense> call() throws Exception {
                Thread.sleep(1000);
                // load data and populate list ...
                expenses = db.loadExpenses();
                return expenses ;
            }
        };
        loadDataTask.setOnSucceeded(e -> exp_table.setItems(loadDataTask.getValue()));
        loadDataTask.setOnFailed(e -> { /* handle errors... */ });

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxWidth(50);
        progressIndicator.setMaxHeight(50);
        progressIndicator.setStyle("-fx-progress-color: #558C89;");
        exp_table.setPlaceholder(progressIndicator);

        Thread loadDataThread = new Thread(loadDataTask);
        loadDataThread.start();
    }

    public void refreshExpenses(){
        initializeExpenses();
    }

    public void setUpExpenses(){
        Callback<TableColumn<Expense, String>, TableCell<Expense, String>> cellFactory =
                new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
                    @Override
                    public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
                        final TableCell<Expense, String> cell = new TableCell<Expense, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    final Button delBtn = new Button("Delete");
                                    final Button editBtn = new Button("Edit");
                                    Expense expense = getTableView().getItems().get(getIndex());
                                    delBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                                    alert.setTitle("Confirmation Dialog");
                                                    alert.setHeaderText(null);
                                                    alert.setContentText("Are you sure you want to delete the expense?");
                                                    Optional<ButtonType> result =alert.showAndWait();
                                                    if(result.get() == ButtonType.OK) {
                                                        DatabaseHandler db = new DatabaseHandler();
                                                        db.deleteExpense(expense.getId());
                                                        initializeExpenses();
                                                    }
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    editBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    editExpense(expense.getId());
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    HBox hBox = new HBox();
                                    hBox.getChildren().addAll(delBtn, editBtn);
                                    hBox.setAlignment(Pos.CENTER);
                                    hBox.setSpacing(5);
                                    setGraphic(hBox);
                                    this.setStyle("-fx-font-weight: normal");
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        exp_action.setCellFactory(cellFactory);

//        exp_amount.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
//
//        exp_date.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
    }

    public void searchExpense(){
        ObservableList<Expense> expenses = FXCollections.observableArrayList();
        if(Checker.isStringInt(searchExpTxt.getText().substring(0,2))){
            for(Expense expense : this.expenses){
                if(expense.getDate().toLowerCase().contains(searchExpTxt.getText())){
                    expenses.add(expense);
                }
            }
            setUpExpenses();
            exp_table.setItems(null);
            exp_table.setItems(expenses);
        }else{
            for(Expense expense : this.expenses){
                if(expense.getName().toLowerCase().contains(searchExpTxt.getText())||expense.getStore().toLowerCase().contains(searchExpTxt.getText())){
                    expenses.add(expense);
                }
            }
            setUpExpenses();
            exp_table.setItems(null);
            exp_table.setItems(expenses);
        }
    }

    public void addExpense(){
        String store = store_choicebox.getValue().toString();
        String name = expense_name.getText();
        String amount_value = amount.getText();
        String desc = description.getText();
        LocalDate date = datePicker.getValue();

        if(store.isEmpty()||name.isEmpty()||amount_value.isEmpty()||date==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Field");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }else{
            db = new DatabaseHandler();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Expense Status");
            alert.setHeaderText(null);

            String[] amountString = amount_value.split(",");
            String amounter = "";
            for(int i=0;i<amountString.length;i++){
                amounter = amounter + amountString[i];
            }
            double amont = Double.parseDouble(amounter);

            boolean result = db.addExpense(store,name,amont,desc,date);
            if(result){
                store_choicebox.setValue("Choose Store");
                expense_name.setText("");
                amount.setText("");
                description.setText("");
                datePicker.setValue(null);
                initializeExpenses();
                alert.setContentText("Expense successful added!");
                alert.showAndWait();
            }else{
                alert.setContentText("Unsuccessful added!");
                alert.showAndWait();
            }
        }
    }

    public void initializeEmployees(){
        emp_no.setCellValueFactory(new PropertyValueFactory<>("emp_no"));
        emp_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        emp_salary.setCellValueFactory(new PropertyValueFactory<>("salary"));
        emp_store.setCellValueFactory(new PropertyValueFactory<>("store"));
        emp_lastpay.setCellValueFactory(new PropertyValueFactory<>("lastpay"));
        emp_status.setCellValueFactory(new PropertyValueFactory<>("status"));
        emp_action.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));

        setUpEmployees();

        DatabaseHandler db = new DatabaseHandler();
        emp_table.setItems(null);

        Task<ObservableList<Employee>> loadDataTask = new Task<ObservableList<Employee>>() {
            @Override
            protected ObservableList<Employee> call() throws Exception {
                Thread.sleep(1500);
                // load data and populate list ...
                return db.loadEmployees();
            }
        };
        loadDataTask.setOnSucceeded(e -> emp_table.setItems(loadDataTask.getValue()));
        loadDataTask.setOnFailed(e -> { /* handle errors... */ });

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxWidth(50);
        progressIndicator.setMaxHeight(50);
        progressIndicator.setStyle("-fx-progress-color: #558C89;");
        emp_table.setPlaceholder(progressIndicator);

        Thread loadDataThread = new Thread(loadDataTask);
        loadDataThread.start();
    }

    public void setUpEmployees(){
        Callback<TableColumn<Employee, String>, TableCell<Employee, String>> cellFactory =
                new Callback<TableColumn<Employee, String>, TableCell<Employee, String>>() {
                    @Override
                    public TableCell<Employee, String> call(TableColumn<Employee, String> param) {
                        final TableCell<Employee, String> cell = new TableCell<Employee, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    final Button payBtn = new Button("Pay");
                                    final Button editBtn = new Button("Edit");
                                    Employee employee = getTableView().getItems().get(getIndex());
                                    payBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    Alert alert = new Alert(Alert.AlertType.WARNING);
                                                    alert.setTitle("Warning Dialog");
                                                    alert.setHeaderText(null);
                                                    alert.setContentText("Are you sure you want to confirm payment of the employee?");
                                                    Optional<ButtonType> result =alert.showAndWait();
                                                    if(result.get() == ButtonType.OK) {
                                                        db = new DatabaseHandler();
                                                        db.updatePay(Integer.parseInt(employee.getEmp_no().split("/")[2]));
                                                        initializeEmployees();
                                                    }
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    editBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    editEmployee(Integer.parseInt(employee.getEmp_no().split("/")[2]));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    HBox hBox = new HBox();
                                    hBox.getChildren().addAll(payBtn, editBtn);
                                    hBox.setAlignment(Pos.CENTER);
                                    hBox.setSpacing(5);
                                    setGraphic(hBox);
                                    this.setStyle("-fx-font-weight: normal");
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        emp_action.setCellFactory(cellFactory);

        emp_status.setCellFactory(new Callback<TableColumn<Employee, String>, TableCell<Employee, String>>() {

            @Override
            public TableCell<Employee, String> call(TableColumn<Employee, String> p) {


                return new TableCell<Employee, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            if(item.equals("not paid")) {
                                this.setStyle("-fx-background-color: #D9853B;-fx-border-color: #FFFFFF;" +
                                        "-fx-border-width: 1px;-fx-text-fill: #FFFFFF;-fx-font-size: 12px;-fx-font-weight: normal;-fx-alignment: center");
                            }
                            else {
                                this.setStyle("-fx-background-color:  #74AFAD;-fx-border-color: #EEEEEE;" +
                                        "-fx-border-width: 1px;-fx-text-fill: #FFFFFF;-fx-font-size: 12px;-fx-font-weight:normal;-fx-alignment: center");
                            }
                            setText(item);
                        }
                    }
                };
            }
        });

//        exp_amount.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
//
//        exp_date.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
    }

    public void registerUser() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        if(user_fname.getText().equals("")||user_lname.getText().equals("")||username.getText().equals("")||user_role_choicebox.getValue().toLowerCase().equals("choose role")){
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }else {
            DatabaseHandler db = new DatabaseHandler();
            boolean result = db.addUser(user_fname.getText(),user_lname.getText(),username.getText(),user_fname.getText().toLowerCase()+"123",((RadioButton)user_genderChoice.getSelectedToggle()).getText(),user_role_choicebox.getValue(),"active");
            if(result){
                alert.setContentText("Successful added!");
                initializeViewUsers();
                user_fname.setText("");
                user_lname.setText("");
                username.setText("");
                user_genderChoice.selectToggle(null);

            }else{
                alert.setContentText("UnSuccessful!");
            }
            alert.showAndWait();
        }

    }

    public void initializeViewUsers() {
        user_no.setCellValueFactory(new PropertyValueFactory<>("user_no"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        user_role.setCellValueFactory(new PropertyValueFactory<>("role"));
        user_status.setCellValueFactory(new PropertyValueFactory<>("status"));
        user_action.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));
        setUpUserTable();
        DatabaseHandler db = new DatabaseHandler();
        user_table.setItems(null);
        try {
            System.out.println(userObject.getString("role"));
            user_table.setItems(db.getUsers(userObject.getString("role")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUpUserTable() {
        Callback<TableColumn<User, String>, TableCell<User, String>> cellFactory =
                new Callback<TableColumn<User, String>, TableCell<User, String>>() {
                    @Override
                    public TableCell<User, String> call(TableColumn<User, String> param) {
                        final TableCell<User, String> cell = new TableCell<User, String>() {
                            final Button statusBtn = new Button("Block");
                            final Button deleteBtn = new Button("Delete");

                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    User user = getTableView().getItems().get(getIndex());
                                    if (user.getStatus().equals("active")) {
                                        statusBtn.setPrefWidth(60);
                                        statusBtn.setText("Block");
                                    } else {
                                        statusBtn.setPrefWidth(60);
                                        statusBtn.setText("Activate");
                                    }
                                    if(user.getUser_no().equals("IS/U/1")){
                                        statusBtn.setVisible(false);
                                        deleteBtn.setVisible(false);
                                    }
                                    statusBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            User user = getTableView().getItems().get(getIndex());
                                            DatabaseHandler db = new DatabaseHandler();
                                            db.updateUserStatus(statusBtn.getText().toLowerCase(),Integer.parseInt(user.getUser_no().substring(5)));
                                            initializeViewUsers();
                                        }
                                    });
                                    deleteBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            Alert alert = new Alert(Alert.AlertType.WARNING);
                                            alert.setTitle("Warning Dialog");
                                            alert.setHeaderText(null);alert.setContentText("Are you sure you want to delete the user?");
                                            Optional<ButtonType> result =alert.showAndWait();
                                            if(result.get() == ButtonType.OK){
                                                User user = getTableView().getItems().get(getIndex());
                                                DatabaseHandler db = new DatabaseHandler();
                                                if(db.deleteUser(Integer.parseInt(user.getUser_no().substring(5)))){
                                                    initializeViewUsers();
                                                }
                                            }
                                        }
                                    });
                                    HBox hBox = new HBox();
                                    hBox.getChildren().addAll(statusBtn, deleteBtn);
                                    hBox.setAlignment(Pos.CENTER);
                                    hBox.setSpacing(5);
                                    setGraphic(hBox);
                                    this.setStyle("-fx-font-weight: normal");
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        user_action.setCellFactory(cellFactory);

        user_status.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {

            @Override
            public TableCell<User, String> call(TableColumn<User, String> p) {


                return new TableCell<User, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            if(item.equals("deactive")) {
                                this.setStyle("-fx-background-color: #D9853B;-fx-border-color: #FFFFFF;" +
                                        "-fx-border-width: 1px;-fx-text-fill: #FFFFFF;-fx-font-size: 12px;-fx-font-weight: normal;-fx-alignment: center");
                            }
                            else {
                                this.setStyle("-fx-background-color:  #74AFAD;-fx-border-color: #EEEEEE;" +
                                        "-fx-border-width: 1px;-fx-text-fill: #FFFFFF;-fx-font-size: 12px;-fx-font-weight:normal;-fx-alignment: center");
                            }
                            setText(item);
                        }
                    }
                };
            }
        });

        user_no.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center-left");
                            setText(item);
                        }
                    }
                };
            }
        });

        user_name.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center-left");
                            setText(item);
                        }
                    }
                };
            }
        });

        user_role.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center");
                            setText(item);
                        }
                    }
                };
            }
        });
    }

    public void addEmployee(){
        String[] amountString = emp_amount.getText().split(",");
        String amounter = "";
        for(int i=0;i<amountString.length;i++){
            amounter = amounter + amountString[i];
        }
        double sal = Double.parseDouble(amounter);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        if(emp_fname.getText().equals("")||emp_lname.getText().equals("")||emp_amount.getText().equals("")||emp_store_choicebox.getValue().toLowerCase().equals("choose store")){
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }else {
            DatabaseHandler db = new DatabaseHandler();
            boolean result = db.addEmployee(emp_fname.getText(),emp_lname.getText(),((RadioButton)emp_genderChoice.getSelectedToggle()).getText(),emp_store_choicebox.getValue(),sal);
            if(result){
                alert.setContentText("Successful added!");
                initializeEmployees();
                emp_fname.setText("");
                emp_lname.setText("");
                emp_amount.setText("");
                emp_store_choicebox.setValue("Choose Store");
                emp_genderChoice.selectToggle(null);

            }else{
                alert.setContentText("UnSuccessful!");
            }
            alert.showAndWait();
        }
    }

    public void editExpense(int id){
        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/expense_edit.fxml"));
            stage.setTitle("Inventory System");
            Scene editCustomerScene = new Scene(fxmlLoader.load(),560,558);
            stage.setScene(editCustomerScene);
            ExpenseEditController memberEditController = fxmlLoader.getController();
            memberEditController.getId(id,this.fxmlLoader);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void editEmployee(int id){
        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/employee_edit.fxml"));
            stage.setTitle("Inventory System");
            Scene editEmployeeScene = new Scene(fxmlLoader.load(),507,392);
            stage.setScene(editEmployeeScene);
            EmployeeEditController employeeEditController = fxmlLoader.getController();
            employeeEditController.getId(id,this.fxmlLoader);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getUserDetails(JSONObject userObject,FXMLLoader fxmlLoader){
        this.userObject = userObject;
        this.fxmlLoader = fxmlLoader;
        initializeViewUsers();
        try {
            if(userObject.getString("role").equals("Admin")){
                ObservableList<String> options = FXCollections.observableArrayList("Choose Role", "Worker");
                user_role_choicebox.setValue("Choose Role");
                user_role_choicebox.setItems(options);
            }else{
                ObservableList<String> options = FXCollections.observableArrayList("Choose Role", "Admin", "Worker");
                user_role_choicebox.setValue("Choose Role");
                user_role_choicebox.setItems(options);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
