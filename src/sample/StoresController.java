package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.json.JSONException;
import org.json.JSONObject;
import sample.Backend.DatabaseHandler;
import sample.Models.Evaluation;
import sample.Models.Purchase;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

public class StoresController implements Initializable{
    @FXML
    public ChoiceBox<String> purchase_choicebox;
    public ChoiceBox<String> evaluation_choicebox;

    @FXML public ChoiceBox<String> product_choicebox;
    @FXML public TextField quantity;
    @FXML public TextField cost;
    @FXML public DatePicker datePicker;
    @FXML public TableView<Purchase> purchases_table;
    @FXML public TableColumn<Purchase,String> purchases_name;
    @FXML public TableColumn<Purchase,String> purchases_quantity;
    @FXML public TableColumn<Purchase,String> purchases_cost;
    @FXML public TableColumn<Purchase,String> purchases_date;
    @FXML public TableColumn purchases_action;
    @FXML public TextField searchPurchaseTxt;

    @FXML public TableView<Evaluation> ev_table;
    @FXML public TableColumn<Evaluation,String> ev_date;
    @FXML public TableColumn<Evaluation,String> ev_no_purchases;
    @FXML public TableColumn<Evaluation,String> ev_expense;
    @FXML public TableColumn<Evaluation,String> ev_income;
    @FXML public TableColumn<Evaluation,String> ev_profit;
    @FXML public TextField searchEvTxt;

    DatabaseHandler db;
    JSONObject userObject;

    ObservableList<Evaluation> evaluations = FXCollections.observableArrayList();
    ObservableList<Purchase> purchases = FXCollections.observableArrayList();

    FXMLLoader fxmlLoader;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        db = new DatabaseHandler();

        purchases_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        purchases_quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        purchases_cost.setCellValueFactory(new PropertyValueFactory<>("amount"));
        purchases_date.setCellValueFactory(new PropertyValueFactory<>("date"));
        purchases_action.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));

        ev_date.setCellValueFactory(new PropertyValueFactory<>("date"));
        ev_no_purchases.setCellValueFactory(new PropertyValueFactory<>("total_purchases"));
        ev_expense.setCellValueFactory(new PropertyValueFactory<>("expenses"));
        ev_income.setCellValueFactory(new PropertyValueFactory<>("income"));
        ev_profit.setCellValueFactory(new PropertyValueFactory<>("profit"));

        ObservableList<String> stores = FXCollections.observableArrayList(db.loadStores());
        stores.add(0,"Choose store");
        purchase_choicebox.setValue("Choose store");
        purchase_choicebox.setItems(stores);
        evaluation_choicebox.setValue(stores.get(0));
        evaluation_choicebox.setItems(stores);

        ObservableList<String> options = FXCollections.observableArrayList("Select product","Crate","Full shell","Bottle");
        product_choicebox.setValue("Select product");
        product_choicebox.setItems(options);

        purchase_choicebox.valueProperty().addListener((observable1, oldValue1, newValue1) -> {
            if(purchase_choicebox.getValue().equals("Choose store")){
                purchases_table.setItems(null);
            }else{
                fillPurchases(purchase_choicebox.getValue());
            }
        });

        evaluation_choicebox.valueProperty().addListener((observable1, oldValue1, newValue1) -> {
            if(evaluation_choicebox.getValue().equals("Choose store")){
                ev_table.setItems(null);
            }else{
                fillEvaluation(evaluation_choicebox.getValue());
            }
        });
    }

    public void addPurchase(){
        String product_choice = product_choicebox.getValue();
        String store_choice = purchase_choicebox.getValue();
        String quantity_text = quantity.getText();
        String cost_text = cost.getText();
        LocalDate purchase_date = datePicker.getValue();

        if(product_choice.isEmpty()||store_choice.isEmpty()||quantity_text.isEmpty()||cost_text.isEmpty()||purchase_date==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Field");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }else{
            if(store_choice.equals("Choose store")){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Store option");
                alert.setHeaderText(null);
                alert.setContentText("Please choose store!");
                alert.showAndWait();
            }else{
                DatabaseHandler db = new DatabaseHandler();

                String[] amountString = cost_text.split(",");
                String amounter = "";
                for(int i=0;i<amountString.length;i++){
                    amounter = amounter + amountString[i];
                }
                double amont = Double.parseDouble(amounter);

                int product = 0;
                if(product_choice.equals("Crate")){
                    product = 1;
                }else if(product_choice.equals("Full shell")){
                    product = 2;
                }else if(product_choice.equals("Bottle")){
                    product = 3;
                }

                Alert alerter = new Alert(Alert.AlertType.CONFIRMATION);
                alerter.setTitle("Store Confirmation");
                alerter.setHeaderText(null);
                alerter.setContentText("Are you sure you want to add purchase to "+purchase_choicebox.getValue()+"!");
                Optional<ButtonType> btn_result = alerter.showAndWait();
                if(btn_result.get() == ButtonType.OK) {
                    boolean result = db.addPurchase(product,store_choice,Integer.parseInt(quantity_text),amont,purchase_date);
                    if(result){
                        product_choicebox.setValue("Select product");
                        purchase_choicebox.setValue("Choose Store");
                        quantity.setText("");
                        cost.setText("");
                        datePicker.setValue(null);

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Registration Status");
                        alert.setHeaderText(null);
                        alert.setContentText("Purchase successful added!");
                        alert.showAndWait();
                        fillPurchases(purchase_choicebox.getValue());
                    }else{
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Registration Status");
                        alert.setHeaderText(null);
                        alert.setContentText("Unsuccessful added!");
                        alert.showAndWait();
                    }
                }
            }

        }
    }

    public void searchPurchase(){
        if(purchase_choicebox.getValue().equals("Choose store")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Store option");
            alert.setHeaderText(null);
            alert.setContentText("Please choose store!");
            alert.showAndWait();
        }else {
            ObservableList<Purchase> purchases = FXCollections.observableArrayList();
            if(Checker.isStringInt(searchPurchaseTxt.getText().substring(0,2))){
                for(Purchase purchase : this.purchases){
                    if(purchase.getDate().contains(searchPurchaseTxt.getText())){
                        purchases.add(purchase);
                    }
                }
                setUpPurchases();
                purchases_table.setItems(null);
                purchases_table.setItems(purchases);
            }else{
                for(Purchase purchase : this.purchases){
                    if(purchase.getName().toLowerCase().contains(searchPurchaseTxt.getText())){
                        purchases.add(purchase);
                    }
                }
                setUpPurchases();
                purchases_table.setItems(null);
                purchases_table.setItems(purchases);
            }
        }
    }

    public void refreshPurchases(){
        if(purchase_choicebox.getValue().equals("Choose store")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Store option");
            alert.setHeaderText(null);
            alert.setContentText("Please choose store!");
            alert.showAndWait();
        }else {
            fillPurchases(purchase_choicebox.getValue());
        }
    }

    public void searchEvaluation(){
        if(evaluation_choicebox.getValue().equals("Choose store")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Store option");
            alert.setHeaderText(null);
            alert.setContentText("Please choose store!");
            alert.showAndWait();
        }else {
            ObservableList<Evaluation> evaluations = FXCollections.observableArrayList();
            for(Evaluation evaluation : this.evaluations){
                if(evaluation.getDate().split("/")[0].toLowerCase().contains(searchEvTxt.getText())||evaluation.getDate().split("/")[1].contains(searchEvTxt.getText())){
                    evaluations.add(evaluation);
                }
            }
            setUpEvaluations();
            ev_table.setItems(null);
            ev_table.setItems(evaluations);
        }
    }

    public void refreshEvaluation(){
        if(evaluation_choicebox.getValue().equals("Choose store")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Store option");
            alert.setHeaderText(null);
            alert.setContentText("Please choose store!");
            alert.showAndWait();
        }else {
            fillEvaluation(evaluation_choicebox.getValue());
        }
    }

    public void setUpPurchases(){
        Callback<TableColumn<Purchase, String>, TableCell<Purchase, String>> cellFactory =
                new Callback<TableColumn<Purchase, String>, TableCell<Purchase, String>>() {
                    @Override
                    public TableCell<Purchase, String> call(TableColumn<Purchase, String> param) {
                        final TableCell<Purchase, String> cell = new TableCell<Purchase, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    final Button delBtn = new Button("Delete");
                                    final Button editBtn = new Button("Edit");
                                    Purchase purchase = getTableView().getItems().get(getIndex());
                                    delBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    Alert alert = new Alert(Alert.AlertType.WARNING);
                                                    alert.setTitle("Warning Dialog");
                                                    alert.setHeaderText(null);
                                                    alert.setContentText("Are you sure you want to delete the purchase?");
                                                    Optional<ButtonType> result =alert.showAndWait();
                                                    if(result.get() == ButtonType.OK) {
                                                        DatabaseHandler db = new DatabaseHandler();
                                                        db.deletePurchase(purchase.getId());
                                                        fillPurchases(purchase_choicebox.getValue());
                                                    }
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    editBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    editPurchase(purchase.getId());
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    HBox hBox = new HBox();
                                    hBox.getChildren().addAll(delBtn, editBtn);
                                    hBox.setAlignment(Pos.CENTER);
                                    hBox.setSpacing(5);
                                    setGraphic(hBox);
                                    this.setStyle("-fx-font-weight: normal");
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        purchases_action.setCellFactory(cellFactory);

//        exp_amount.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
//
//        exp_date.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
    }

    public void fillPurchases(String store_name){

        DatabaseHandler db = new DatabaseHandler();

        setUpPurchases();

        purchases_table.setItems(null);

        Task<ObservableList<Purchase>> loadDataTask = new Task<ObservableList<Purchase>>() {
            @Override
            protected ObservableList<Purchase> call() throws Exception {
                // load data and populate list ...
                purchases = db.loadPurchases(store_name);
                return purchases ;
            }
        };
        loadDataTask.setOnSucceeded(e ->
        {if(loadDataTask.getValue().size()>0){
            purchases_table.setItems(loadDataTask.getValue());
        }else {
            purchases_table.setItems(null);
        }});
        loadDataTask.setOnFailed(e -> {/*Handle Errors */});

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxWidth(50);
        progressIndicator.setMaxHeight(50);
        progressIndicator.setStyle("-fx-progress-color: #558C89;");
        purchases_table.setPlaceholder(progressIndicator);

        Thread loadDataThread = new Thread(loadDataTask);
        loadDataThread.start();
    }

    public void fillEvaluation(String store_name){

        DatabaseHandler db = new DatabaseHandler();

        setUpEvaluations();

        ev_table.setItems(null);

        Task<ObservableList<Evaluation>> loadDataTask = new Task<ObservableList<Evaluation>>() {
            @Override
            protected ObservableList<Evaluation> call() throws Exception {
                // load data and populate list ...
                evaluations = db.evaluation(store_name);
                return  evaluations;
            }
        };
        loadDataTask.setOnSucceeded(e ->
        {if(loadDataTask.getValue().size()>0){
            ev_table.setItems(loadDataTask.getValue());
        }else {
            ev_table.setItems(null);
        }});
        loadDataTask.setOnFailed(e -> {/*Handle Errors */});

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxWidth(50);
        progressIndicator.setMaxHeight(50);
        progressIndicator.setStyle("-fx-progress-color: #558C89;");
        ev_table.setPlaceholder(progressIndicator);

        Thread loadDataThread = new Thread(loadDataTask);
        loadDataThread.start();
    }

    public void setUpEvaluations(){

        ev_date.setCellFactory(new Callback<TableColumn<Evaluation, String>, TableCell<Evaluation, String>>() {
            @Override
            public TableCell<Evaluation, String> call(TableColumn<Evaluation, String> param) {
                return new TableCell<Evaluation, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center");
                            setText(item);
                        }
                    }
                };
            }
        });

        ev_no_purchases.setCellFactory(new Callback<TableColumn<Evaluation, String>, TableCell<Evaluation, String>>() {
            @Override
            public TableCell<Evaluation, String> call(TableColumn<Evaluation, String> param) {
                return new TableCell<Evaluation, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center");
                            setText(item);
                        }
                    }
                };
            }
        });

        ev_expense.setCellFactory(new Callback<TableColumn<Evaluation, String>, TableCell<Evaluation, String>>() {
            @Override
            public TableCell<Evaluation, String> call(TableColumn<Evaluation, String> param) {
                return new TableCell<Evaluation, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center");
                            setText(item);
                        }
                    }
                };
            }
        });

        ev_income.setCellFactory(new Callback<TableColumn<Evaluation, String>, TableCell<Evaluation, String>>() {
            @Override
            public TableCell<Evaluation, String> call(TableColumn<Evaluation, String> param) {
                return new TableCell<Evaluation, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center");
                            setText(item);
                        }
                    }
                };
            }
        });

        ev_profit.setCellFactory(new Callback<TableColumn<Evaluation, String>, TableCell<Evaluation, String>>() {
            @Override
            public TableCell<Evaluation, String> call(TableColumn<Evaluation, String> param) {
                return new TableCell<Evaluation, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            if(item.startsWith("-"))
                                this.setStyle("-fx-text-fill: red;-fx-alignment: center");
                            else
                                this.setStyle("-fx-alignment: center");
                            setText(item);
                        }
                    }
                };
            }
        });
    }

    public void editPurchase(int id){
        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/purchase_edit.fxml"));
            stage.setTitle("Inventory System");
            Scene editPurchaseScene = new Scene(fxmlLoader.load(),492,391);
            stage.setScene(editPurchaseScene);
            PurchaseEditController purchaseEditController = fxmlLoader.getController();
            purchaseEditController.getId(id,purchase_choicebox.getValue(),this.fxmlLoader);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getUserDetails(JSONObject userObject,FXMLLoader fxmlLoader){
        this.userObject = userObject;
        this.fxmlLoader = fxmlLoader;
        try {
            System.out.println("The role is"+userObject.getString("role"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
