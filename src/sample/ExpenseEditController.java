package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.Models.Expense;
import sample.Backend.DatabaseHandler;

import java.time.LocalDate;

public class ExpenseEditController {

    @FXML public TextField earning_name;
    @FXML public TextField amount;
    @FXML public TextArea description;
    @FXML public DatePicker datePicker;
    @FXML public ChoiceBox<String> store_choicebox;

    DatabaseHandler db;
    int id;
    String c_name,c_amount,c_description;

    FXMLLoader fxmlLoader;

    public void getId(int id, FXMLLoader fxmlLoader){
        db = new DatabaseHandler();
        Expense expense = db.viewExpense(id);
        this.id = id;
        this.fxmlLoader = fxmlLoader;
        earning_name.setText(expense.getName());
        amount.setText(expense.getAmount());
        description.setText(expense.getDescription());
        String[] date = expense.getDate().split("-");
        datePicker.setValue(LocalDate.of(Integer.parseInt(date[0]),Integer.parseInt(date[1]),Integer.parseInt(date[2])));

        ObservableList<String> stores = FXCollections.observableArrayList(db.loadStores());
        store_choicebox.setValue(expense.getStore());
        store_choicebox.setItems(stores);

        earning_name.requestFocus();
    }

    public void editExpense(){
        c_name = earning_name.getText();
        c_amount = amount.getText();
        c_description = description.getText();
        LocalDate date = datePicker.getValue();

        String[] amountString = c_amount.split(",");
        String amounter = "";
        for(int i=0;i<amountString.length;i++){
            amounter = amounter + amountString[i];
        }
        double amont = Double.parseDouble(amounter);

        DatabaseHandler db = new DatabaseHandler();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        if(db.editExpense(id,c_name,store_choicebox.getValue(),amont,c_description,date)){
            alert.setContentText("Successful Edited!");
        }else {
            alert.setContentText("UnSuccessful try Again!");
        }
        alert.showAndWait();
        ManagementController managementController = fxmlLoader.getController();
        managementController.initializeExpenses();
        Stage stage = (Stage) earning_name.getScene().getWindow();
        stage.close();
    }
}
