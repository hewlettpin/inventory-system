package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Backend.DatabaseHandler;

import java.time.LocalDate;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("fxml/login.fxml"));
        primaryStage.setTitle("Pepsi Inventory");
        primaryStage.setScene(new Scene(root, 1153, 674));
        primaryStage.show();
//        String compareDate = "'"+ LocalDate.now().plusMonths(2).withDayOfMonth(1).minusDays(1).toString()+"'";
//        System.out.println(compareDate);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
