package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.Backend.DatabaseHandler;
import sample.Models.Expense;
import sample.Models.PurchaseEdit;

import java.time.LocalDate;

public class PurchaseEditController {

    @FXML public TextField quantity;
    @FXML public TextField cost;
    @FXML public DatePicker datePicker;
    @FXML public ChoiceBox<String> product_choicebox;

    DatabaseHandler db;
    int id;

    FXMLLoader fxmlLoader;
    String store_name;
    int quantity_nxt;

    public void getId(int id,String store_name,FXMLLoader fxmlLoader){
        db = new DatabaseHandler();
        PurchaseEdit purchaseEdit = db.viewPurchase(id);
        this.id = id;
        this.store_name = store_name;
        this.fxmlLoader = fxmlLoader;
        quantity_nxt = Integer.parseInt(purchaseEdit.quantity);
        quantity.setText(purchaseEdit.quantity);
        cost.setText(purchaseEdit.cost);
        String[] date = purchaseEdit.date.split("-");
        datePicker.setValue(LocalDate.of(Integer.parseInt(date[0]),Integer.parseInt(date[1]),Integer.parseInt(date[2])));

        ObservableList<String> products = FXCollections.observableArrayList("Crate","Full shell","Bottle");
        product_choicebox.setValue(purchaseEdit.name);
        product_choicebox.setItems(products);

        quantity.requestFocus();
    }

    public void editPurchase(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);

        if(quantity.getText().isEmpty()||cost.getText().isEmpty()){
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }else {
            LocalDate date = datePicker.getValue();

            String[] amountString = cost.getText().split(",");
            String amounter = "";
            for(int i=0;i<amountString.length;i++){
                amounter = amounter + amountString[i];
            }
            double amont = Double.parseDouble(amounter);

            int extra;
            String state;
            if(quantity_nxt>Integer.parseInt(quantity.getText())){
                extra = quantity_nxt - Integer.parseInt(quantity.getText());
                state = "sub";
            }else{
                extra = Integer.parseInt(quantity.getText()) - quantity_nxt;
                state = "add";
            }
            DatabaseHandler db = new DatabaseHandler();
            if(db.editPurchase(id,product_choicebox.getValue(),Integer.parseInt(quantity.getText()),amont,date,store_name,state,extra)){
                alert.setContentText("Successful Edited!");
                alert.showAndWait();
                StoresController storesController = fxmlLoader.getController();
                storesController.fillPurchases(store_name);
            }else {
                alert.setContentText("UnSuccessful try Again!");
                alert.showAndWait();
            }
            Stage stage = (Stage) quantity.getScene().getWindow();
            stage.close();
        }

    }
}
