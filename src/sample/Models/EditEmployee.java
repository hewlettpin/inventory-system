package sample.Models;

public class EditEmployee {
    public String fname,lname,salary,store;

    public EditEmployee(String fname, String lname, String salary, String store) {
        this.fname = fname;
        this.lname = lname;
        this.salary = salary;
        this.store = store;
    }
}
