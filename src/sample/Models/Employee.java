package sample.Models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Employee {
    public final StringProperty emp_no;
    public final StringProperty name;
    public final StringProperty status;
    public final StringProperty salary;
    public final StringProperty store;
    public final StringProperty lastpay;

    public Employee(String emp_no, String name, String salary, String store, String lastpay,String status) {
        this.emp_no = new SimpleStringProperty(emp_no);
        this.name = new SimpleStringProperty(name);
        this.status = new SimpleStringProperty(status);
        this.salary = new SimpleStringProperty(salary);
        this.store = new SimpleStringProperty(store);
        this.lastpay = new SimpleStringProperty(lastpay);
    }

    public String getEmp_no() {
        return emp_no.get();
    }

    public StringProperty emp_noProperty() {
        return emp_no;
    }

    public void setEmp_no(String emp_no) {
        this.emp_no.set(emp_no);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getSalary() {
        return salary.get();
    }

    public StringProperty salaryProperty() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary.set(salary);
    }

    public String getStore() {
        return store.get();
    }

    public StringProperty storeProperty() {
        return store;
    }

    public void setStore(String store) {
        this.store.set(store);
    }

    public String getLastpay() {
        return lastpay.get();
    }

    public StringProperty lastpayProperty() {
        return lastpay;
    }

    public void setLastpay(String lastpay) {
        this.lastpay.set(lastpay);
    }
}
