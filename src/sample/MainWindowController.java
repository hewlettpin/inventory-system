package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {
    @FXML public HBox home;
    @FXML public HBox stores;
    @FXML public HBox sales;
    @FXML public HBox admin;
    @FXML public BorderPane borderPane;

    @FXML public MenuButton profBtn;

    JSONObject userCredentials;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuItem menuItem1 = new MenuItem("Change Password");
        MenuItem menuItem2 = new MenuItem("Log out");
        profBtn.getItems().addAll(menuItem1,menuItem2);
        menuItem1.setOnAction(event -> {
            try {
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("fxml/change_password.fxml"));
                stage.setTitle("Pepsi Inventory");
                Scene changePasswordScene = new Scene(fxmlLoader1.load(),475,315);
                stage.setScene(changePasswordScene);
                ChangePasswordController changePasswordController = fxmlLoader1.getController();
                changePasswordController.getUserDetails(userCredentials);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        menuItem2.setOnAction(event -> {
            try {
                Stage stage = (Stage) profBtn.getScene().getWindow();
                Parent root = FXMLLoader.load(getClass().getResource("fxml/login.fxml"));
                stage.setTitle("Pepsi Inventory");
                stage.setMaximized(false);
                Scene loginScene = new Scene(root,1153, 674);
                stage.setScene(loginScene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void onHomeClick(){
        home.setStyle("-fx-background-color:  #424242");
        stores.setStyle("-fx-background-color:  #2d373c");
        sales.setStyle("-fx-background-color:  #2d373c");
        admin.setStyle("-fx-background-color:  #2d373c");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/home.fxml"));
        try {
            borderPane.setCenter(fxmlLoader.load());
            HomeController homeController = fxmlLoader.getController();
            homeController.getUserDetails(userCredentials,fxmlLoader);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStoresClick(){
        try {
            if(userCredentials.getString("role").equals("Main Admin")||userCredentials.getString("role").equals("Admin")){
                stores.setStyle("-fx-background-color:  #424242");
                home.setStyle("-fx-background-color:  #2d373c");
                sales.setStyle("-fx-background-color:  #2d373c");
                admin.setStyle("-fx-background-color:  #2d373c");
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/stores.fxml"));
                try {
                    borderPane.setCenter(fxmlLoader.load());
                    StoresController storesController = fxmlLoader.getController();
                    storesController.getUserDetails(userCredentials,fxmlLoader);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onManageClick(){
        try {
            if(userCredentials.getString("role").equals("Main Admin")||userCredentials.getString("role").equals("Admin")){
                sales.setStyle("-fx-background-color:  #424242");
                home.setStyle("-fx-background-color:  #2d373c");
                stores.setStyle("-fx-background-color:  #2d373c");
                admin.setStyle("-fx-background-color:  #2d373c");
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/management.fxml"));
                try {
                    borderPane.setCenter(fxmlLoader.load());
                    ManagementController managementController = fxmlLoader.getController();
                    managementController.getUserDetails(userCredentials,fxmlLoader);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onAdminClick(){
        try {
            if(userCredentials.getString("role").equals("Main Admin")||userCredentials.getString("role").equals("Admin")){
                admin.setStyle("-fx-background-color:  #424242");
                home.setStyle("-fx-background-color:  #2d373c");
                stores.setStyle("-fx-background-color:  #2d373c");
                sales.setStyle("-fx-background-color:  #2d373c");
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/admin.fxml"));
                try {
                    borderPane.setCenter(fxmlLoader.load());
                    AdminController adminController = fxmlLoader.getController();
                    adminController.getUserDetails(userCredentials);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getUserDetails(JSONObject userCredentials){
        try {
            this.userCredentials = userCredentials;
            profBtn.setText("Hi! "+userCredentials.getString("fname"));
            home.setStyle("-fx-background-color:  #424242");
            stores.setStyle("-fx-background-color:  #2d373c");
            sales.setStyle("-fx-background-color:  #2d373c");
            admin.setStyle("-fx-background-color:  #2d373c");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/home.fxml"));
            try {
                borderPane.setCenter(fxmlLoader.load());
                HomeController homeController = fxmlLoader.getController();
                homeController.getUserDetails(userCredentials,fxmlLoader);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
