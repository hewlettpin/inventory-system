package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.json.JSONException;
import org.json.JSONObject;
import sample.Backend.DatabaseHandler;
import sample.Models.Store;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class AdminController implements Initializable {

    @FXML public TextField new_store_text;

    @FXML public ChoiceBox change_choicebox;
    @FXML public TextField new_price;

    @FXML public TableView<Store>  stores_table;
    @FXML public TableColumn<Store,Integer> store_num;
    @FXML public TableColumn<Store,String> store_name;

    DatabaseHandler db;
    ObservableList<String> options;
    JSONObject userObject;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        db = new DatabaseHandler();

        initializeStoreTable();

        options = FXCollections.observableArrayList("Select product","Crate","Full shell","Bottle");
        change_choicebox.setValue("Select product");
        change_choicebox.setItems(options);
    }

    private void initializeStoreTable(){
        store_num.setCellValueFactory(new PropertyValueFactory<>("id"));
        store_name.setCellValueFactory(new PropertyValueFactory<>("name"));

        setUpStoreTable();

        DatabaseHandler db = new DatabaseHandler();
        stores_table.setItems(null);
        stores_table.setItems(db.loadStoresList());
    }

    private void setUpStoreTable(){
        store_num.setCellFactory(new Callback<TableColumn<Store, Integer>, TableCell<Store, Integer>>() {
            @Override
            public TableCell<Store, Integer> call(TableColumn<Store, Integer> param) {
                return new TableCell<Store, Integer>() {

                    @Override
                    public void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center");
                            setText(String.valueOf(item));
                        }
                    }
                };
            }
        });

        store_name.setCellFactory(new Callback<TableColumn<Store, String>, TableCell<Store, String>>() {
            @Override
            public TableCell<Store, String> call(TableColumn<Store, String> param) {
                return new TableCell<Store, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            this.setStyle("-fx-alignment: center");
                            setText(item);
                        }
                    }
                };
            }
        });
    }

    public void addStore(){
        String store = new_store_text.getText();
        boolean success;

        if(!store.isEmpty()){
            db = new DatabaseHandler();
            success =  db.addStore(store);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registration Status");
            alert.setHeaderText(null);
            if(success){
                new_store_text.setText("");
                alert.setContentText("Successful Added!");
                alert.showAndWait();
                initializeStoreTable();
            }else {
                alert.setContentText("Unsuccessful!");
                alert.showAndWait();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Field");
            alert.setHeaderText(null);
            alert.setContentText("Please fill the Store name!");
            alert.showAndWait();
        }
    }

    public void changePrice(){
        String price = new_price.getText();
        String product = change_choicebox.getValue().toString();
        boolean success;

        if(!product.isEmpty()||!price.isEmpty()){

            String[] amountString = price.split(",");
            String amounter = "";
            for(int i=0;i<amountString.length;i++){
                amounter = amounter + amountString[i];
            }
            double amont = Double.parseDouble(amounter);

            int product_id = 0;
            if(product.equals("Crate")){
                product_id = 1;
            }else if(product.equals("Full shell")){
                product_id = 2;
            }else if(product.equals("Bottle")){
                product_id = 3;
            }

            db = new DatabaseHandler();
            success =  db.changePrice(amont,product_id);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Price Change");
            alert.setHeaderText(null);
            if(success){
                change_choicebox.setValue("Select product");
                new_price.setText("");
                alert.setContentText("Price Successful changed!");
                alert.showAndWait();
            }else {
                alert.setContentText("Price change Unsuccessful!");
                alert.showAndWait();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Field");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all fields!");
            alert.showAndWait();
        }
    }

    public void getUserDetails(JSONObject userObject){
        this.userObject = userObject;
        try {
            System.out.println("The role is"+userObject.getString("role"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
