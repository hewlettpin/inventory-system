package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.json.JSONException;
import org.json.JSONObject;
import sample.Backend.DatabaseHandler;
import sample.Models.Sale;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

public class HomeController implements Initializable{
    @FXML public ChoiceBox<String> home_choicebox;
    @FXML public Label crate_price;
    @FXML public Label bottle_price;
    @FXML public Label full_shell_price;
    @FXML public Label crates_count;

    @FXML public ChoiceBox<String> product_choicebox;
    @FXML public TextField quantity;
    @FXML public DatePicker datePicker;
    @FXML public TableView<Sale> sales_table;
    @FXML public TableColumn<Sale,String> sales_name;
    @FXML public TableColumn<Sale,String> sales_quantity;
    @FXML public TableColumn<Sale,String> sales_cost;
    @FXML public TableColumn<Sale,String> sales_date;
    @FXML public TableColumn sales_action;
    @FXML public TextField searchSaleTxt;

    DatabaseHandler db;
    JSONObject userObject;
    JSONObject prices;
    FXMLLoader fxmlLoader;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sales_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        sales_quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        sales_cost.setCellValueFactory(new PropertyValueFactory<>("amount"));
        sales_date.setCellValueFactory(new PropertyValueFactory<>("date"));
        sales_action.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));

        db = new DatabaseHandler();

        prices = db.getPrices();

        try {
            crate_price.setText(String.format("%,.0f", prices.getDouble("crate"))+"/=");
            bottle_price.setText(String.format("%,.0f", prices.getDouble("bottle")) +"/=");
            full_shell_price.setText(String.format("%,.0f", prices.getDouble("full shell")) +"/=");
        }catch (JSONException e) {
            e.printStackTrace();
        }

        ObservableList<String> options = FXCollections.observableArrayList("Select product","Crate","Full shell","Bottle");
        product_choicebox.setValue("Select product");
        product_choicebox.setItems(options);

        ObservableList<String> stores = FXCollections.observableArrayList(db.loadStores());
        stores.add(0,"Choose store");
        home_choicebox.setValue("Choose store");
        home_choicebox.setItems(stores);

        home_choicebox.valueProperty().addListener((observable1, oldValue1, newValue1) -> {
            if(home_choicebox.getValue().equals("Choose store")){
                sales_table.setItems(null);
                crates_count.setText("");
            }else{
                fillSales(home_choicebox.getValue());
            }
        });
    }

    public void fillSales(String store_name){

        DatabaseHandler db = new DatabaseHandler();

        crates_count.setText(String.valueOf(db.getQuantity(home_choicebox.getValue())));

        setUpSales();

        sales_table.setItems(null);

        Task<ObservableList<Sale>> loadDataTask = new Task<ObservableList<Sale>>() {
            @Override
            protected ObservableList<Sale> call() throws Exception {
//                Thread.sleep(1000);
                // load data and populate list ...
                return db.loadSales(store_name) ;
            }
        };
        loadDataTask.setOnSucceeded(e ->
        {if(loadDataTask.getValue().size()>0){
            sales_table.setItems(loadDataTask.getValue());
        }else {
            sales_table.setItems(null);
        }});
        loadDataTask.setOnFailed(e -> {/*Handle Errors */});

        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxWidth(50);
        progressIndicator.setMaxHeight(50);
        progressIndicator.setStyle("-fx-progress-color: #558C89;");
        sales_table.setPlaceholder(progressIndicator);

        Thread loadDataThread = new Thread(loadDataTask);
        loadDataThread.start();
    }

    public void setUpSales(){
        Callback<TableColumn<Sale, String>, TableCell<Sale, String>> cellFactory =
                new Callback<TableColumn<Sale, String>, TableCell<Sale, String>>() {
                    @Override
                    public TableCell<Sale, String> call(TableColumn<Sale, String> param) {
                        final TableCell<Sale, String> cell = new TableCell<Sale, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    final Button delBtn = new Button("Delete");
                                    final Button editBtn = new Button("Edit");
                                    Sale sale = getTableView().getItems().get(getIndex());
                                    delBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    Alert alert = new Alert(Alert.AlertType.WARNING);
                                                    alert.setTitle("Warning Dialog");
                                                    alert.setHeaderText(null);
                                                    alert.setContentText("Are you sure you want to delete the sale?");
                                                    Optional<ButtonType> result =alert.showAndWait();
                                                    if(result.get() == ButtonType.OK) {
                                                        DatabaseHandler db = new DatabaseHandler();
                                                        db.deleteSale(sale.getId());
                                                        fillSales(home_choicebox.getValue());
                                                    }
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    editBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            try {
                                                if(userObject.getString("role").equals("Main Admin")||userObject.getString("role").equals("Admin")){
                                                    editSale(sale.getId());
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    HBox hBox = new HBox();
                                    hBox.getChildren().addAll(delBtn, editBtn);
                                    hBox.setAlignment(Pos.CENTER);
                                    hBox.setSpacing(5);
                                    setGraphic(hBox);
                                    this.setStyle("-fx-font-weight: normal");
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        sales_action.setCellFactory(cellFactory);

//        exp_amount.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
//
//        exp_date.setCellFactory(new Callback<TableColumn<Expense, String>, TableCell<Expense, String>>() {
//            @Override
//            public TableCell<Expense, String> call(TableColumn<Expense, String> param) {
//                return new TableCell<Expense, String>() {
//
//                    @Override
//                    public void updateItem(String item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (!isEmpty()) {
//                            this.setStyle("-fx-alignment: center");
//                            setText(item);
//                        }
//                    }
//                };
//            }
//        });
    }

    public void addSale(){
        String store = home_choicebox.getValue();
        String product = product_choicebox.getValue();
        int quantity_txt = Integer.parseInt(quantity.getText());
        LocalDate date = datePicker.getValue();

        if(store.equals("Choose store")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Store option");
            alert.setHeaderText(null);
            alert.setContentText("Please choose store!");
            alert.showAndWait();
        }else{
            if(product.equals("Select product")||quantity.getText().isEmpty()||date==null){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Empty Field");
                alert.setHeaderText(null);
                alert.setContentText("Please fill all fields!");
                alert.showAndWait();
            }else{
                DatabaseHandler db = new DatabaseHandler();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sale Add");
                alert.setHeaderText(null);

                if(quantity_txt>db.getQuantity(store)){
                    alert.setContentText("No enough crates in store!");
                    alert.showAndWait();
                }else{
                    double cost = 0;
                    int product_id = 0;
                    try{
                        if(product.equals("Crate")){
                            cost = quantity_txt*prices.getDouble("crate");
                            product_id = 1;
                        }else if(product.equals("Full shell")){
                            cost = quantity_txt*prices.getDouble("full shell");
                            product_id = 2;
                        }else if(product.equals("Bottle")){
                            cost = quantity_txt*prices.getDouble("bottle");
                            product_id = 3;
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                    boolean result = db.addSale(product_id,store,quantity_txt,cost,date);
                    if(result){
                        product_choicebox.setValue("Select product");
                        quantity.setText("");
                        datePicker.setValue(null);
                        fillSales(home_choicebox.getValue());
                        alert.setContentText("Sale successful added!");
                        alert.showAndWait();
                    }else{
                        alert.setContentText("Unsuccessful added!");
                        alert.showAndWait();
                    }
                }

            }
        }
    }

    public void searchSale(){
        ObservableList<Sale> sales = FXCollections.observableArrayList();
        DatabaseHandler db = new DatabaseHandler();
        if(Checker.isStringInt(searchSaleTxt.getText().substring(0,2))){
            for(Sale sale : db.loadSales(home_choicebox.getValue())){
                if(sale.getDate().toLowerCase().contains(searchSaleTxt.getText())){
                    sales.add(sale);
                }
            }
            setUpSales();
            sales_table.setItems(null);
            sales_table.setItems(sales);
        }else{
            for(Sale sale : db.loadSales(home_choicebox.getValue())){
                if(sale.getName().toLowerCase().contains(searchSaleTxt.getText())){
                    sales.add(sale);
                }
            }
            setUpSales();
            sales_table.setItems(null);
            sales_table.setItems(sales);
        }
    }

    public void refreshSales(){
        fillSales(home_choicebox.getValue());
    }

    public void editSale(int id){
        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/sale_edit.fxml"));
            stage.setTitle("Inventory System");
            Scene editPurchaseScene = new Scene(fxmlLoader.load(),492,391);
            stage.setScene(editPurchaseScene);
            SaleEditController saleEditController = fxmlLoader.getController();
            saleEditController.getId(id,home_choicebox.getValue(),this.fxmlLoader);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getUserDetails(JSONObject userObject,FXMLLoader fxmlLoader){
        this.userObject = userObject;
        this.fxmlLoader = fxmlLoader;
        try {
            System.out.println("The role is"+userObject.getString("role"));
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
